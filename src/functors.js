// @flow

// define generic operation processor.
const operation = (values: Array<number>, callback: Function): number => {
  let result: number = values[0];
  for(let i: number = 1, c: number = values.length; i < c; ++i){
    result = callback(result, values[i]);
  }
  return result;
};

// define `add` operation function.
export const add = (values: Array<number>): number =>
  operation(values, (a: number, b: number) => a + b);

// define `sub` operation function.
export const sub = (values: Array<number>): number =>
  operation(values, (a: number, b: number) => a - b);

// define `mul` operation function.
export const mul = (values: Array<number>): number =>
  operation(values, (a: number, b: number) => a * b);

// define `div` operation function.
export const div = (values: Array<number>): number =>
  operation(values, (a: number, b: number) => a / b);
