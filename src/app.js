// @flow

// import modules.
import { getOperationStateFromArgv } from "./data-processors";

// import types.
import type { State } from "./data-processors";

// prepare data.
const state: State | null = getOperationStateFromArgv(process.argv);

// process data.
if(!state){
  throw new Error('Could not get valid operation state!');
}
if(state.operation){
  console.log(state.operation(state.values));
}else{
  throw new Error('Cannot resolve operation!');
}
