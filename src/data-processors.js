// @flow

// import modules.
import * as functors from "./functors";

// define enumerations.
const ArgModes = {
  None: 'None',
  Operation: 'Operation',
  Values: 'Values'
}
type EnumArgModes = $Keys<typeof ArgModes>;

// define operation state type.
export type State = {
  operation: Function | null,
  values: Array<number>
};

// define local data processor that takes operation ID as input
// and returns operation functor that will process values.
const acquireOperation = (operation: string): Function | null =>
  functors[operation] || null; // return null (not undefined) if don't exists.

// define data processor that takes process argv as input
// and returns operation state.
export const getOperationStateFromArgv = (args: Array<string>): State => {
  let argMode: EnumArgModes = ArgModes.None;

  let operation: Function | null = null;
  const values: Array<number> = [];

  args.forEach((arg: string) => {
    if(argMode === ArgModes.None){
      if(arg === '--operation' || arg === '-o'){
        argMode = ArgModes.Operation;
      }else if(arg === '--'){
        argMode = ArgModes.Values;
      }
    }else if(argMode === ArgModes.Operation){
      operation = acquireOperation(arg);
      argMode = ArgModes.None;
    }else if(argMode === ArgModes.Values){
      values.push(parseFloat(arg));
    }else{
      throw new Error('Unknown argument case: ' + arg);
    }
  });

  if(values.length < 1){
    throw new Error('You should provide at least one value!');
  }

  return {
    operation,
    values
  }
};
